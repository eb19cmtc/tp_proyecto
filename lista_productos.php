<?php
	session_start();
	if(!isset($_SESSION['validacion']))
	{
		header("location:inicio.php");
		die();
	}
?>

<link rel="stylesheet" type="text/css" href="css/inicioStyle.css">

<div class="mydiv">
	<br><br>
	<a class="btnInicio" href="inicio.php">Inicio</a>

	<a class="btnMenuTienda" href="menu_tienda.php">Regresar al menu</a>

	<form action="php/cerrar_sesion_tienda.php" method="POST">
		<button class="LogoutAdmin" type="submit">Cerrar sesion</button>
	</form>

</div>

<img class="logo"src="imagenes/logo.jpg">

<div class="ListaProductos">
<br>
<h2>Lista de Productos</h2>

<form action="lista_productos.php" method="GET">
	<select name="categoria">
	<?php
		require("php/AbrirConexion.php");

		$sql = "SELECT * FROM categorias";

		$consulta = mysqli_query($conexion,$sql);

		$cate = "<option value=0 >Elegir categoria</option>";

		while($result = mysqli_fetch_assoc($consulta))
		{
			$cate .= "<option value=".$result['idcategoria']."> ".$result['nombre']."</option>";

		}
		echo $cate;

		require("php/CerrarConexion.php");
	?>
	</select>
	<button type="submit">Buscar productos</button>
</form>

<?php
	if(!empty($_GET['categoria']))
	{
		$categoria_get = $_GET['categoria'];
		require("php/AbrirConexion.php");

		$sql_select = "SELECT * FROM productos WHERE idcategoria='$categoria_get'";

		$consulta_select = mysqli_query($conexion,$sql_select);

		while($productos = mysqli_fetch_assoc($consulta_select))
		{ ?>
			<table id="Customers">
				<tr>
					<td>
						ID
					</td>
					<td>
						Nombre>
					</td>
					<td>
						Precio
					</td>
					<td>
						Stock
					</td>
					<td>
						Marca
					</td>
				</tr>
				<tr>
					<td>
						<?php echo $productos['idproducto']?>
					</td>
					<td>
						<?php echo $productos['nombre']?>
					</td>
					<td>
						<?php echo $productos['precio_unidad']?>
					</td>
					<td>
						<?php echo $productos['stock']?>
					</td>
					<td>
						<?php echo $productos['MARCA']?>
					</td>
				</tr>

				</table>
		<?php }

		require("php/CerrarConexion.php");
	}

?>

</div>
