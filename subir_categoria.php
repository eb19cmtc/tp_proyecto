<?php
	session_start();
	if(!isset($_SESSION['validacion']))
	{
		header("location:inicio.php");
		die();
	}
?>

<link rel="stylesheet" type="text/css" href="css/inicioStyle.css">

<div class="mydiv">
	<br><br>
	<a class="btnInicio" href="inicio.php">Inicio</a>

	<a class="btnMenuTienda" href="menu_tienda.php">Regresar al menu</a>

	<form action="php/cerrar_sesion_tienda.php" method="POST">
		<button class="LogoutAdmin" type="submit">Cerrar sesion</button>
	</form>

</div>

<img class="logo"src="imagenes/logo.jpg">

<div class="divAltaCategorias">

<?php
	if(empty($_GET['repetido']))
	{

	}
	else
	{
		echo "categoria repetida";
	}
?>

<br>

<form action="php/subir_categoria.php" method="POST">
	<p style="color: #FA9403"><label>Nombre de categoria</label></p>
	<input type="text" autocomplete="off" name="categoria">
	<button type="submit">Subir</button>
</form>

</div>
