<?php
	session_start();
	if(!isset($_SESSION['validacion']))
	{
		header("location:inicio.php");
		die();
	}
?>

<head>

<link rel="stylesheet" type="text/css" href="css/inicioStyle.css">

<title>Agregar producto</title>

</head>

<br>

<div class="mydiv">
	<br><br>
	<a class="btnInicio" href="inicio.php">Inicio</a>

	<a class="btnMenuTienda" href="menu_tienda.php">Regresar al menu</a>

	<form action="php/cerrar_sesion_tienda.php" method="POST">
		<button class="LogoutAdmin" type="submit">Cerrar sesion</button>
	</form>

</div>

<img class="logo"src="imagenes/logo.jpg">

<br><br>

<div class="divAltaProductos">

<form action="subir_producto.php" method="POST">
	<table>
		<tr>
			<th>Categoria</th>
			<th>Nombre</th>
			<th>Precio</th>
			<th>Stock</th>
			<th>Marca</th>
			<th>Imagen</th>
		</tr>
		<tr>
			<th><select name="categoria" required="">
					<?php
						require("php/AbrirConexion.php");

						$sql = "SELECT * FROM categorias";

						$consulta = mysqli_query($conexion,$sql);

						$cate = "<option value=0 >Elegir categoria</option>";

						while($result = mysqli_fetch_assoc($consulta))
						{
							$cate .= "<option value=".$result['idcategoria']."> ".$result['nombre']."</option>";

						}
						echo $cate;

						require("php/CerrarConexion.php");
					?>
  				</select></th>
			<th><input autocomplete="off" type="text" name="nombre" required=""></th>
			<th><input autocomplete="off" type="number" name="precio" required=""></th>
			<th><input autocomplete="off" type="number" name="stock" required=""></th>
			<th><input autocomplete="off" type="text" name="marca" required=""></th>
			<th><input autocomplete="off" type="text" name="imagen" required=""></th>
		</tr>
	</table>
	<br><br>
	<button class="Subir" type="submit" name="bt1"><p>Subir</p></button>
</form>
<?php
	if(isset($_POST['bt1']))
	{
		require("php/AbrirConexion.php");

		$categoria = $_POST['categoria'];
		$nombre = $_POST['nombre'];
		$precio = $_POST['precio'];
		$stock = $_POST['stock'];
		$marca = $_POST['marca'];
		$imagen = $_POST['imagen'];

		$sql_select = "SELECT `nombre` FROM productos WHERE `nombre` = '$nombre'";
		$sql_insert = "INSERT INTO productos(`idproducto`,`nombre`,`precio_unidad`,`stock`,`MARCA`,`idcategoria`,`imagen`)VALUES(DEFAULT ,'$nombre','$precio','$stock','$marca','$categoria','$imagen')";

		$consulta_select = mysqli_query($conexion,$sql_select);
		$contador_select = mysqli_num_rows($consulta_select);

		if($contador_select == 1)
		{
			$salida = "Ya existe el producto ".$nombre;
		}
		elseif($contador_select == 0)
		{
			$consulta_insert = mysqli_query($conexion,$sql_insert);
			$salida = "Se ingreso el producto ".$nombre;
		}
		else
		{
			$salida = "Algo salio mal";
		}
		require("php/CerrarConexion.php");

		?>

		<script type="text/javascript">
	 	alert("<?php echo $salida; ?>");
	 	</script>

	 	<?php
	}
?>
</div>
