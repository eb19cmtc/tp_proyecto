<?php
	session_start();
	if(!isset($_SESSION['validacion']))
	{
		header("location:inicio.php");
		die();
	}
?>

<link rel="stylesheet" type="text/css" href="css/inicioStyle.css">

<div class="mydiv">
	<br><br>
	<a class="btnInicio" href="inicio.php">Inicio</a>

	<a class="btnMenuTienda" href="menu_tienda.php">Regresar al menu</a>

	<form action="php/cerrar_sesion_tienda.php" method="POST">
		<button class="LogoutAdmin" type="submit">Cerrar sesion</button>
	</form>

</div>

<img class="logo"src="imagenes/logo.jpg">

<div class="divLogin">

<form action="php/editar_producto.php" method="POST">

	<p><label>ID</label></p>
	<input type="number" name="id">
	<p><label>Nombre</label></p>
	<input type="text" name="nombre">
	<p><label>Precio</label></p>
	<input type="number" name="precio">
	<p><label>Stock</label></p>
	<input type="number" name="stock">
	<p><label>Marca</label></p>
	<input type="text" name="marca">

	<br><br>

	<button type="submit">Editar producto</button>
</form>

</div>
