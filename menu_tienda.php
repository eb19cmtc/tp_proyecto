<?php
	session_start();
	if(!isset($_SESSION['validacion']))
	{
		header("location:inicio.php");
		die();
	}
?>

<link rel="stylesheet" type="text/css" href="css/inicioStyle.css">

<br>

<div class="mydiv">
	<br><br>
	<a class="btnInicio" href="inicio.php">Inicio</a>

	<form action="php/cerrar_sesion_tienda.php" method="POST">
		<button class="LogoutAdmin" type="submit">Cerrar sesion</button>
	</form>

</div>

<img class="logo"src="imagenes/logo.jpg">

<div class="divMenuProductos">
<a class="tiposProductos" href="subir_producto.php">Subir producto</a>
<a class="tiposProductos" href="subir_categoria.php">Subir categoria</a>
<a class="tiposProductos" href="lista_productos.php">Lista productos</a>
<a class="tiposProductos" href="editar_producto.php">Editar productos</a>
</div>
