-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-11-2021 a las 13:38:29
-- Versión del servidor: 10.4.8-MariaDB
-- Versión de PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mydb`
--

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`idcategoria`, `nombre`) VALUES
(1, 'Ram'),
(2, 'Fuentes'),
(3, 'Tarjeta graficas'),
(4, 'Motherboars');

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nombre`, `password`, `direccion`, `cod_prov`, `email`, `telefono`, `dni`) VALUES
(1, 'Brayan', 'b148e7f41fdc3be4b14e8d17e068bbad', 'asda', 'asdasd', 'asdas', '123', '2313');

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`idproducto`, `nombre`, `precio_unidad`, `stock`, `MARCA`, `idcategoria`, `imagen`) VALUES
(1, '8 ram', '22333', 1231, 'Hyperx', 1, 'https://www.guarconsa.com/assets/images/image-not-found.png'),
(2, 'zxas', '1234', 123, 'asxz', 1, 'https://www.guarconsa.com/assets/images/image-not-found.png'),
(3, 'dasdadasd', '21312', 12312, '3sdadasd', 1, 'https://www.guarconsa.com/assets/images/image-not-found.png'),
(4, '312331asdadada', '1313', 21312, 'vcadasd', 1, 'https://www.guarconsa.com/assets/images/image-not-found.png'),
(5, '3123saddadada', '1231', 3123123, 'sdzxccvvz', 1, 'https://www.guarconsa.com/assets/images/image-not-found.png'),
(6, 'vxcvxcvxv', '231', 12312, 'cvxvxcv', 1, 'https://www.guarconsa.com/assets/images/image-not-found.png'),
(7, '7FSDFSDFSDS', '123123', 312313, '3123123', 1, 'https://www.guarconsa.com/assets/images/image-not-found.png'),
(8, '8dasdadasdas', '123123', 3123, '13231312', 1, 'https://www.guarconsa.com/assets/images/image-not-found.png'),
(9, '9sddasdad', '123123', 123123, 'scfscvcb', 1, 'https://www.guarconsa.com/assets/images/image-not-found.png'),
(10, '550W PLUS BRONCE', '8000', 15, 'Corsair', 2, 'https://www.guarconsa.com/assets/images/image-not-found.png');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
