<?php
	if(count($_GET)==1)
	{
		die();
	}
	else if (count($_GET)>2)
	{
		die();
	}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>

		<link rel="stylesheet" type="text/css" href="css/inicioStyle.css">

</head>
<body>
<br>

<div class="mydiv">
	<br><br>
	<a class="btnInicio" href="inicio.php">Inicio</a>

	<?php
		session_start();

		if(!isset($_SESSION['session']))
		{
			?>
			<a class="Login" href="iniciar_sesion.php">Iniciar Sesión</a>;
			<?php
		}
		else
		{
			?>
			<a class="Carrito" href="">Carrito</a>;
			<?php

			?>
			<a class="Logout" href="php/cerrar_sesion.php">Cerrar Sesión</a>;
			<?php
		}
	?>
</div>


<img class="logo"src="imagenes/logo.jpg">

<div class="menuIzquierda">

	<?php
		include("php/AbrirConexion.php");

		$sql = "SELECT * FROM categorias";
		$consulta = mysqli_query($conexion,$sql);

		echo "<table>";

		while ($resultado = mysqli_fetch_array($consulta))
		{
			echo "<tr>";
	    	echo "<td>";
	    	?>
				<br><br><br><br>

	    	<a class="tiposProductos" href="categoria.php?categoria=<?php echo $resultado['idcategoria'] ?>&pagina=1"><?php echo $resultado['nombre'] ?></a>;
	    	<?php
	    	echo "</td>";
	    	echo "</tr>";
		}

		echo "</table>";

		include("php/CerrarConexion.php");
	?>
</div>
	<br>
<div class="menuProductos">

	<br>

	<h1 style="color:#FA9403">Productos</h1>

	<?php

	if(isset($_GET['categoria']))
	{
		define("PRODUCTOS_X",8);

		if(isset($_GET['pagina']))
		{
			$pagina = $_GET['pagina'];
			$comienzo = PRODUCTOS_X*($pagina-1);
			$final=$comienzo+PRODUCTOS_X;
		}


		include("php/AbrirConexion.php");
		$idcategoria = $_GET['categoria'];
		$sql = "SELECT productos.`nombre`, productos.`precio_unidad`, productos.`stock`, productos.`MARCA` , productos.`imagen` FROM productos WHERE idcategoria = $idcategoria";
		$consulta = mysqli_query($conexion,$sql);
		$consulta_max_productos = mysqli_num_rows($consulta);
		$total_paginas = ceil($consulta_max_productos/PRODUCTOS_X);

		$productos = mysqli_fetch_all($consulta);

		if($final<$consulta_max_productos)
		{
			for($j = $comienzo;$j<$final;$j++)
			{
				$producto=$productos[$j];
				?>

				<table id="Customers">
					<tr>
						<td>
							<img src="<?php echo $producto['4'];?>" alt='imagen no encontrada' width='90' heigth='40'>
						</td>
						<td>
							<?php echo "Nombre: ".$producto['0']?>
						</td>

						<td>
							<?php echo "Precio x Unidad: ".$producto['1']?>
						</td>
						<td>
							<?php echo "Stock disponible: ".$producto['2']?>
						</td>
						<td>
							<?php echo "Marca: ".$producto['3']?>
						</td>

					</tr>

				</table>

				<?php
			}
		}

		else
		{
			for($j = $comienzo;$j<$consulta_max_productos;$j++)
			{
				$producto=$productos[$j];
				?>
				<table id="Customers">
					<tr>
						<td>
							<?php echo $producto['0']?>
						</td>
						<td>
							<?php echo $producto['1']?>
						</td>
						<td>
							<?php echo $producto['2']?>
						</td>
						<td>
							<?php echo $producto['3']?>
						</td>
						<td>
							<img src="<?php echo $producto['4'];?>" alt='imagen no encontrada' width='90' heigth='40'>
						</td>
					</tr>

				</table>

				<?php
			}
		}

		$contener_hrefs="";

		for($i=1;$i<=$total_paginas;$i++)
		{
			$contener_hrefs .= "<a href='categoria.php?categoria=$idcategoria&pagina=$i'>$i</a>";

		}

		include("php/CerrarConexion.php");

		echo "<br>";
		echo $contener_hrefs;
	}



?>

</div>
<br>
<img class="sponsors" src="imagenes/sponsors.png">
</body>
</html>
